import { Error, Model, FilterQuery, HydratedDocument } from 'mongoose';
import { IMongoService } from 'repository/IMongoService.ts';
import { injectable } from 'inversify';
import { catchError, mergeMap, Observable, of } from 'rxjs';
import { ResponseMapping } from 'infrastructure/mapping/ResponseMapping.ts';
import { HttpStatus } from 'infrastructure/enum/HttpStatus.ts';
import { ErrorMapping } from 'infrastructure/mapping/ErrorMapping.ts';

@injectable()
export class MongoService implements IMongoService {
  public findOne<K = object, T = object>(
    model: Model<K>,
    filterQuery: FilterQuery<T>,
  ): Observable<T> {
    return of(1).pipe(
      mergeMap(async (): Promise<T> => model.findOne(filterQuery) as T),
    );
  }

  public findAll<K = object, T = object>(
    model: Model<K>,
    filterQuery?: FilterQuery<T>,
    key?: string,
  ): Observable<ResponseMapping> {
    return of(1).pipe(
      mergeMap(
        async (): Promise<T[]> => model.find(filterQuery!) as unknown as T[],
      ),
      mergeMap((data: unknown[]) => of(data as any)),
      catchError((err) => of(new Error.CastError('', err, ''))),
      mergeMap((err: Error.CastError) =>
        of(this._validateResponse(err, undefined, key)),
      ),
    );
  }

  public save<K = object, T = object>(
    model: Model<K>,
    dto: T,
    message?: string,
  ): Observable<ResponseMapping> {
    const document: HydratedDocument<K> = new model({ ...dto });

    return of(1).pipe(
      mergeMap(async (): Promise<any> => await document.save()),
      catchError((err) => of(new Error.ValidatorError(err))),
      mergeMap((err: Error.ValidatorError) =>
        of(this._validateResponse(err, message)),
      ),
    );
  }

  private _validateResponse(
    err: Error,
    body?: string | object,
    key?: string,
  ): ResponseMapping {
    if (err instanceof Error.CastError) {
      const { value }: ErrorMapping = JSON.parse(JSON.stringify(err));

      return {
        statusCode: HttpStatus.BadRequest,
        body: value,
        key: '',
      };
    }

    if (err instanceof Error.ValidatorError) {
      const {
        properties: { errors },
      }: ErrorMapping = JSON.parse(JSON.stringify(err));

      return {
        statusCode: HttpStatus.BadRequest,
        body: errors,
        key: '',
      };
    }

    return {
      statusCode: HttpStatus.OK,
      body: !body ? err : body,
      key: !key ? '' : key,
    };
  }
}
