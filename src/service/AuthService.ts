import { IAuthService } from 'repository/IAuthService.ts';
import { inject, injectable } from 'inversify';
import { lastValueFrom, mergeMap, of } from 'rxjs';
import { MongoService } from 'service/MongoService.ts';
import { ResponseMapping } from 'infrastructure/mapping/ResponseMapping.ts';
import { Test } from 'schemas/test.ts';

@injectable()
export class AuthService implements IAuthService {
  constructor(@inject(MongoService) private _mongoService: MongoService) {}

  public auth(user: any): Promise<ResponseMapping> {
    return lastValueFrom(
      of(1).pipe(mergeMap(() => this._mongoService.save(Test, user))),
    );
  }
}
