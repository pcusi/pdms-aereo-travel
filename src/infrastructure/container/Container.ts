import { Container } from 'inversify';
import { AuthController } from 'controller/auth.controller.ts';
import { AuthService } from 'service/AuthService.ts';
import { MongoService } from 'service/MongoService.ts';

const CONT_APP: Container = new Container();
CONT_APP.bind<AuthController>(AuthController).toSelf();
CONT_APP.bind<AuthService>(AuthService).toSelf();
CONT_APP.bind<MongoService>(MongoService).toSelf();

export default CONT_APP;
