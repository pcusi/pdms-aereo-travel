import mongoose, { Mongoose } from 'mongoose';
import * as dotenv from 'dotenv';

dotenv.config();

export const mongo_db: Promise<Mongoose> = mongoose.connect(
  `${process.env.PDMS_MONGO_URI}/${process.env.PDMS_STAGE}-${process.env.PDMS_MONGO_DB}`,
  {},
);
