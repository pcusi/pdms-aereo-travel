export interface ErrorMapping {
  name: string;
  message: string;
  properties: {
    errors: {
      [k: string]: {
        path: string;
        kind: string;
        message: string;
      };
    };
  };
  value: {
    path: string;
    valueType: string;
    name: string;
  };
}
