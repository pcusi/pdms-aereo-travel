export interface ResponseMapping {
  statusCode: number;
  body: string | number | Array<any> | object | boolean | undefined;
  key: string;
}
