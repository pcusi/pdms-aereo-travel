import { ResponseMapping } from 'infrastructure/mapping/ResponseMapping.ts';

export interface IAuthService {
  auth(user: any): Promise<ResponseMapping>;
}
