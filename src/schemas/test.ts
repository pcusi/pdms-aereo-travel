import { Model, model, Schema } from 'mongoose';

const TestSchema: Schema<any> = new Schema<any>(
  {
    name: {
      type: String,
      required: true,
    },
  },
  { minimize: false },
);

export const Test: Model<any> = model<any>('Test', TestSchema);
