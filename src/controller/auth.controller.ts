import {
  controller,
  httpPost,
  interfaces,
  request,
  response,
} from 'inversify-express-utils';
import { inject } from 'inversify';
import { AuthService } from '../service/AuthService.ts';
import { Request, Response } from 'express';
import { ResponseMapping } from 'infrastructure/mapping/ResponseMapping.ts';

@controller('/auth')
export class AuthController implements interfaces.Controller {
  constructor(@inject(AuthService) private _authService: AuthService) {}

  @httpPost('/')
  public async auth(
    @request() req: Request,
    @response() res: Response,
  ): Promise<Response> {
    const data: any = req.body;

    const response: ResponseMapping = await this._authService.auth(data);

    return res.status(response.statusCode).json({ message: response.body });
  }
}
