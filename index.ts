import 'reflect-metadata';

import { InversifyExpressServer } from 'inversify-express-utils';
import express, { Application } from 'express';

import CONT_APP from 'infrastructure/container/Container';
import { mongo_db } from 'infrastructure/container/Mongo.ts';

const server: InversifyExpressServer = new InversifyExpressServer(
  CONT_APP,
  null,
  { rootPath: '/api/v1' },
);

server.setConfig((app: Application) => {
  app.use(
    express.urlencoded({
      extended: true,
    }),
  );
  app.use(express.json());
});

server.build().listen(3000, async () => {
  console.log('Server started on http://localhost:3000');
  mongo_db.then(() => console.log('MongoDB connected'));
});
